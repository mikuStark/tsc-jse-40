package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.dto.Project;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {
    // extends AbstractService
    public ProjectService(IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.clear();
            sqlSession.commit();
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<Project> collection) {
        if (collection == null) return;
        for (Project i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public Project add(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.add(
                    project.getId(),
                    project.getName(),
                    project.getDescription(),
                    project.getStatus().toString(),
                    prepare(project.getStartDate()),
                    prepare(project.getFinishDate()),
                    project.getCreated(),
                    project.getUserId()
            );
            sqlSession.commit();
            return project;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            add(project);
            sqlSession.commit();
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            add(project);
            sqlSession.commit();
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @Override
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (project == null) throw new ProjectNotFoundException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeByIdUserId(userId, project.getId());
            sqlSession.commit();
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public Project updateById(@NonNull String userId, @NonNull String id,
                              @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);

            Project project = projectRepository.findByIdUserId(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            projectRepository.update(
                    project.getId(),
                    name,
                    description,
                    project.getStatus().toString(),
                    prepare(project.getStartDate()),
                    prepare(project.getFinishDate()),
                    project.getCreated(),
                    project.getUserId()
            );
            sqlSession.commit();
            return project;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @NonNull
    @SneakyThrows
    public Project findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findByNameUserId(userId, name);
        } finally {
            sqlSession.close();
        }
    }
}
