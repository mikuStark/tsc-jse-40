package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.dto.User;

import java.util.Collection;
import java.util.List;

public interface IAdminUserService {

    void addAll(Collection<User> users);

    boolean isLoginExists(String login);

//    boolean isEmailExists(String email);

    List<User> findAll();

    User findByEmail(@NonNull String email);

    User findByLogin(String login);

    User removeUser(User user);

//    void removeByLogin(String login);

    User create(String login, String password);

    @SneakyThrows
    User create(@NonNull String login, @NonNull String password, @NonNull Role role);

    User create(String login, String password, String email);

//    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

    @SneakyThrows
    void clear();

    @SneakyThrows
    User add(User user);
}
