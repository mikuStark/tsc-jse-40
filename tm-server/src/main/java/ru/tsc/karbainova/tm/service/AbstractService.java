package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IService;
import ru.tsc.karbainova.tm.dto.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NonNull
    protected final IConnectionService connectionService;

    protected AbstractService(@NonNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
