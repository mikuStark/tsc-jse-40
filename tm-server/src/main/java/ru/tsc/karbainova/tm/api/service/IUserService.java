package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.dto.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    void addAll(Collection<User> users);

    List<User> findAll();

    User findByLogin(String login);

    @SneakyThrows
    User findById(@NonNull String id);

    User findByEmail(@NonNull String email);

    User create(String login, String password);

    User create(String login, String password, String email);

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );

    User setPassword(String userId, String password);

    @SneakyThrows
    User add(User user);

    @SneakyThrows
    void clear();
}
